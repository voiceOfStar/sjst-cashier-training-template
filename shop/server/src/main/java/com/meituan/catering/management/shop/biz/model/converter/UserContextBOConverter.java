package com.meituan.catering.management.shop.biz.model.converter;

import com.meituan.catering.management.common.model.biz.UserContextBO;

/**
 * @ClassName UserContextBOConverter
 * @Description
 * @Author 唐程君
 * @Date 2022/5/17 10:38
 */
public class UserContextBOConverter {
    public static UserContextBO toUserContextBO(Long tenantId,Long userId){
        UserContextBO userContextBO = new UserContextBO();
        userContextBO.setTenantId(tenantId);
        userContextBO.setUserId(userId);
        return userContextBO;
    }
}
