package com.meituan.catering.management.shop.biz.model.converter;

import com.meituan.catering.management.shop.api.http.model.request.SearchShopHttpRequest;
import com.meituan.catering.management.shop.biz.model.request.SearchShopBizRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName SearchShopBizRequestConverter
 * @Description
 * @Author 唐程君
 * @Date 2022/5/16 10:52
 */
public class SearchShopBizRequestConverter {
    public static SearchShopBizRequest toSearchShopBizRequest(SearchShopHttpRequest request){
        SearchShopBizRequest searchShopBizRequest = new SearchShopBizRequest();
        searchShopBizRequest.setPageIndex(request.getPageIndex());
        searchShopBizRequest.setPageSize(request.getPageSize());
        searchShopBizRequest.setCondition(buildCondition(request.getCondition()));
        searchShopBizRequest.setSortFields(buildSortField(request.getSortFields()));

        return searchShopBizRequest;
    }

    public static SearchShopBizRequest.Condition buildCondition(SearchShopHttpRequest.Condition condition){
        SearchShopBizRequest.Condition conditions = new SearchShopBizRequest.Condition();
        conditions.setKeyword(condition.getKeyword());
        conditions.setManagementType(condition.getManagementType());
        conditions.setBusinessType(condition.getBusinessType());
        conditions.setEnabled(condition.getEnabled());
        return conditions;
    }

    public static List<SearchShopBizRequest.SortField> buildSortField(List<SearchShopHttpRequest.SortField> fields){
        List<SearchShopBizRequest.SortField> newFields = new ArrayList<>();
        for (SearchShopHttpRequest.SortField field:fields){
            SearchShopBizRequest.SortField sortField = new SearchShopBizRequest.SortField();
            sortField.setField(field.getField());
            sortField.setAsc(field.getAsc());
            newFields.add(sortField);
        }
        return newFields;
    }
}
