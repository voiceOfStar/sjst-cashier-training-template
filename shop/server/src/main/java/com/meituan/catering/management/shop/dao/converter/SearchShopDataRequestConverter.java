package com.meituan.catering.management.shop.dao.converter;

import com.meituan.catering.management.shop.biz.model.request.SearchShopBizRequest;
import com.meituan.catering.management.shop.dao.model.request.SearchShopDataRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName SearchShopDataRequestConverter
 * @Description
 * @Author 唐程君
 * @Date 2022/5/16 13:54
 */
public class SearchShopDataRequestConverter {
    public static SearchShopDataRequest toSearchShopDataRequest(Long tenantId, Long userId,SearchShopBizRequest bizRequest){
        SearchShopDataRequest searchShopDataRequest = new SearchShopDataRequest();
        searchShopDataRequest.setPageIndex((bizRequest.getPageIndex()-1)* bizRequest.getPageSize());
        searchShopDataRequest.setPageSize(bizRequest.getPageSize());
        searchShopDataRequest.setKeyword("%"+bizRequest.getCondition().getKeyword()+"%");
        searchShopDataRequest.setManagementType(bizRequest.getCondition().getManagementType());
        searchShopDataRequest.setBusinessType(bizRequest.getCondition().getBusinessType());
        searchShopDataRequest.setEnabled(bizRequest.getCondition().getEnabled());
        searchShopDataRequest.setSortFields(buildSortField(bizRequest.getSortFields()));
        searchShopDataRequest.setUserId(userId);
        searchShopDataRequest.setTenantId(tenantId);
        return searchShopDataRequest;
    }

    public static List<SearchShopDataRequest.SortField> buildSortField(List<SearchShopBizRequest.SortField> fields){
        List<SearchShopDataRequest.SortField> sortFields = new ArrayList<>();
        for (SearchShopBizRequest.SortField field:fields){
            SearchShopDataRequest.SortField sortField = new SearchShopDataRequest.SortField();
            sortField.setField(field.getField());
            sortField.setDirection(field.getAsc() ? "ASC":"DESC");
            sortFields.add(sortField);
        }
        return sortFields;
    }
}
