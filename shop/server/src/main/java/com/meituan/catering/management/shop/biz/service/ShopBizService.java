package com.meituan.catering.management.shop.biz.service;

import com.meituan.catering.management.common.exception.BizException;
import com.meituan.catering.management.common.model.biz.PageBO;
import com.meituan.catering.management.common.model.biz.UserContextBO;
import com.meituan.catering.management.shop.biz.model.ShopBO;
import com.meituan.catering.management.shop.biz.model.request.SaveShopBizRequest;
import com.meituan.catering.management.shop.biz.model.request.SearchShopBizRequest;
import com.meituan.catering.management.shop.biz.model.request.UpdateShopBizRequest;

/**
 * 门店管理服务
 * @author mac
 */
public interface ShopBizService {

    /**
     * 根据商户号查询门店
     * @param tenantId
     * @param userId
     * @param businessNo
     * @return
     */
    ShopBO findByBusinessNo(Long tenantId, Long userId, String businessNo);

    /**
     * 创建门店
     * @param tenantId
     * @param userId
     * @param saveShopBizRequest
     * @return
     */
    ShopBO create(Long tenantId, Long userId, SaveShopBizRequest saveShopBizRequest);

    /**
     * 分页查询门店
     * @param tenantId
     * @param userId
     * @param request
     * @return
     */
    PageBO searchForPage(Long tenantId, Long userId, SearchShopBizRequest request);

    /**
     * 更新门店信息
     * @param userContextBO
     * @param businessNo
     * @param request
     * @return
     * @throws BizException
     */
    ShopBO update(UserContextBO userContextBO, String businessNo, UpdateShopBizRequest request) throws BizException;

    /**
     * 关闭门店
     * @param userContextBO
     * @param businessNo
     * @return
     * @throws BizException
     */
    ShopBO close(UserContextBO userContextBO, String businessNo) throws BizException;

    /**
     * 启用门店
     * @param userContextBO
     * @param businessNo
     * @return
     * @throws BizException
     */
    ShopBO open(UserContextBO userContextBO, String businessNo) throws BizException;
}
