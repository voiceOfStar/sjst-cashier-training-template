package com.meituan.catering.management.shop.dao.converter;

import com.meituan.catering.management.common.model.biz.UserContextBO;
import com.meituan.catering.management.shop.biz.model.request.SaveShopBizRequest;
import com.meituan.catering.management.shop.biz.model.request.UpdateShopBizRequest;
import com.meituan.catering.management.shop.dao.model.ShopDO;

import java.util.Date;

/**
 * @author mac
 */
public class ShopDOConverter {
    public static ShopDO toShopDO(Long tenantId, Long userId, SaveShopBizRequest saveShopBizRequest) {
        ShopDO shopDO = new ShopDO();
        shopDO.setBusinessNo(String.valueOf(System.currentTimeMillis()));
        shopDO.setName(saveShopBizRequest.getName());
        shopDO.setBusinessType(saveShopBizRequest.getBusinessType());
        shopDO.setManagementType(saveShopBizRequest.getManagementType());
        shopDO.setContactTelephone(saveShopBizRequest.getContact().getTelephone());
        shopDO.setContactCellphone(saveShopBizRequest.getContact().getCellphone());
        shopDO.setContactName(saveShopBizRequest.getContact().getName());
        shopDO.setContactAddress(saveShopBizRequest.getContact().getAddress());
        shopDO.setOpeningHoursOpenTime(saveShopBizRequest.getOpeningHours().getOpenTime());
        shopDO.setOpeningHoursCloseTime(saveShopBizRequest.getOpeningHours().getCloseTime());
        shopDO.setBusinessArea(saveShopBizRequest.getBusinessArea());
        shopDO.setComment(saveShopBizRequest.getComment());
        shopDO.setEnabled(Boolean.TRUE);
        shopDO.setTenantId(tenantId);
        shopDO.setCreatedBy(userId);
        shopDO.setCreatedAt(new Date());
        shopDO.setLastModifiedBy(userId);
        shopDO.setLastModifiedAt(new Date());
        return shopDO;
    }

    public static ShopDO toShopDO(UserContextBO userContextBO, String businessNo, UpdateShopBizRequest request) {
        ShopDO shopDO = new ShopDO();
        shopDO.setBusinessNo(businessNo);
        shopDO.setName(request.getName());
        shopDO.setBusinessType(request.getBusinessType());
        shopDO.setManagementType(request.getManagementType());
        shopDO.setContactTelephone(request.getContact().getTelephone());
        shopDO.setContactCellphone(request.getContact().getCellphone());
        shopDO.setContactName(request.getContact().getName());
        shopDO.setContactAddress(request.getContact().getAddress());
        shopDO.setOpeningHoursOpenTime(request.getOpeningHours().getOpenTime());
        shopDO.setOpeningHoursCloseTime(request.getOpeningHours().getCloseTime());
        shopDO.setBusinessArea(request.getBusinessArea());
        shopDO.setComment(request.getComment());
        shopDO.setLastModifiedBy(userContextBO.getUserId());
        shopDO.setTenantId(userContextBO.getTenantId());
        shopDO.setCreatedBy(userContextBO.getUserId());
        shopDO.setLastModifiedAt(new Date());
        shopDO.setVersion(request.getVersion());
        return shopDO;
    }

    public static ShopDO toShopDO(UserContextBO userContextBO, String businessNo,Boolean close){
        ShopDO shopDO = new ShopDO();
        shopDO.setEnabled(close);
        shopDO.setBusinessNo(businessNo);
        shopDO.setLastModifiedBy(userContextBO.getUserId());
        shopDO.setCreatedBy(userContextBO.getUserId());
        shopDO.setLastModifiedAt(new Date());
        shopDO.setTenantId(userContextBO.getTenantId());
        return shopDO;
    }
}
