package com.meituan.catering.management.shop.biz.validator;

import com.meituan.catering.management.common.exception.BizException;
import com.meituan.catering.management.common.model.biz.UserContextBO;
import com.meituan.catering.management.common.model.enumeration.ErrorCode;
import com.meituan.catering.management.shop.api.http.model.request.CloseShopHttpRequest;
import com.meituan.catering.management.shop.api.http.model.request.CreateShopHttpRequest;
import com.meituan.catering.management.shop.api.http.model.request.OpenShopHttpRequest;
import com.meituan.catering.management.shop.api.http.model.request.UpdateShopHttpRequest;
import com.meituan.catering.management.shop.biz.model.request.SaveShopBizRequest;
import com.meituan.catering.management.shop.dao.mapper.ShopMapper;
import com.meituan.catering.management.shop.dao.model.ShopDO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 验证各种请求参数是否正确
 * @author 唐程君
 */
@Component
public class ShopBizServiceValidator {
    @Resource
    private ShopMapper shopMapper;

    public void createValid(UserContextBO userContextBO, CreateShopHttpRequest createShopHttpRequest) throws BizException {
        //以下为示例
        if (userContextBO.getUserId() < 0 || userContextBO.getTenantId() < 0) {
            throw new BizException(ErrorCode.PARAM_ERROR);
        }
    }

    public void updateValid(UserContextBO userContextBO, String businessNo, UpdateShopHttpRequest request) throws BizException {
        if (userContextBO.getUserId() < 0 || userContextBO.getTenantId() < 0) {
            throw new BizException(ErrorCode.PARAM_ERROR);
        }
        ShopDO shopDO = shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo);
        if (!shopDO.getVersion().equals(request.getVersion())) {
            throw new BizException(ErrorCode.VERSION_ERROR);
        }
    }

    public void closeValid(UserContextBO userContextBO, String businessNo, CloseShopHttpRequest request) throws BizException {
        if (userContextBO.getUserId() < 0 || userContextBO.getTenantId() < 0) {
            throw new BizException(ErrorCode.PARAM_ERROR);
        }
        ShopDO shopDO = shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo);
        if (!shopDO.getVersion().equals(request.getVersion())) {
            throw new BizException(ErrorCode.VERSION_ERROR);
        }
    }

    public void openValid(UserContextBO userContextBO, String businessNo, OpenShopHttpRequest request) throws BizException {
        if (userContextBO.getUserId() < 0 || userContextBO.getTenantId() < 0) {
            throw new BizException(ErrorCode.PARAM_ERROR);
        }
        ShopDO shopDO = shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo);
        if (!shopDO.getVersion().equals(request.getVersion())) {
            throw new BizException(ErrorCode.VERSION_ERROR);
        }
    }
}
