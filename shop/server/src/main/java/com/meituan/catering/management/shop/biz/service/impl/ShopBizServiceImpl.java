package com.meituan.catering.management.shop.biz.service.impl;

import com.meituan.catering.management.common.exception.BizException;
import com.meituan.catering.management.common.model.biz.PageBO;
import com.meituan.catering.management.common.model.biz.UserContextBO;
import com.meituan.catering.management.common.model.enumeration.ErrorCode;
import com.meituan.catering.management.shop.biz.model.ShopBO;
import com.meituan.catering.management.shop.biz.model.converter.ShopBOConverter;
import com.meituan.catering.management.shop.biz.model.request.SaveShopBizRequest;
import com.meituan.catering.management.shop.biz.model.request.SearchShopBizRequest;
import com.meituan.catering.management.shop.biz.model.request.UpdateShopBizRequest;
import com.meituan.catering.management.shop.biz.service.ShopBizService;
import com.meituan.catering.management.shop.dao.converter.SearchShopDataRequestConverter;
import com.meituan.catering.management.shop.dao.converter.ShopDOConverter;
import com.meituan.catering.management.shop.dao.mapper.ShopMapper;
import com.meituan.catering.management.shop.dao.model.ShopDO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.util.List;

/**
 * {@link ShopBizService}的核心实现
 * @author 唐程君
 */
@Service
public class ShopBizServiceImpl implements ShopBizService {

    @Resource
    private TransactionTemplate transactionTemplate;

    @Resource
    private ShopMapper shopMapper;

    @Override
    public ShopBO findByBusinessNo(Long tenantId, Long userId, String businessNo) {
        return ShopBOConverter.toShopBO(shopMapper.findByBusinessNo(tenantId,userId,businessNo));
    }

    @Override
    public ShopBO create(Long tenantId, Long userId, SaveShopBizRequest saveShopBizRequest) {
        ShopDO shopDO= ShopDOConverter.toShopDO(tenantId,userId,saveShopBizRequest);
        int id=shopMapper.insert(shopDO);
        Assert.isTrue(id>0,"插入数据库错误");
        return ShopBOConverter.toShopBO(shopMapper.selectById(shopDO.getId()));
    }

    @Override
    public PageBO searchForPage(Long tenantId, Long userId, SearchShopBizRequest request) {
        PageBO<ShopBO> pageBO = new PageBO<>();
        List<ShopDO> shopDOS = shopMapper.searchForPage(SearchShopDataRequestConverter.toSearchShopDataRequest(tenantId,userId,request));
        for(ShopDO shopDO:shopDOS){
            pageBO.getRecords().add(ShopBOConverter.toShopBO(shopDO));
        }
        pageBO.setTotalCount(shopMapper.countAll(SearchShopDataRequestConverter.toSearchShopDataRequest(tenantId, userId, request)));
        pageBO.setPageIndex(request.getPageIndex());
        pageBO.setPageSize(request.getPageSize());
        pageBO.setTotalPageCount((int) Math.ceil((float)pageBO.getTotalCount()/ pageBO.getPageSize()));
        return pageBO;
    }

    @Override
    public ShopBO update(UserContextBO userContextBO, String businessNo, UpdateShopBizRequest request) throws BizException {
        int updateResult = shopMapper.updateByBusinessNo(ShopDOConverter.toShopDO(userContextBO, businessNo, request));
        if (updateResult !=1){
            throw new BizException(63004,"操作失败");
        }else {
            return ShopBOConverter.toShopBO(shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo));
        }
    }

    @Override
    public ShopBO close(UserContextBO userContextBO, String businessNo) throws BizException {
        int closeResult = shopMapper.closeShop(ShopDOConverter.toShopDO(userContextBO,businessNo,false));
        if (closeResult !=1){
            throw new BizException(63004,"操作失败");
        }else {
            return ShopBOConverter.toShopBO(shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo));
        }
    }

    @Override
    public ShopBO open(UserContextBO userContextBO, String businessNo) throws BizException {
        int openResult = shopMapper.openShop(ShopDOConverter.toShopDO(userContextBO,businessNo,true));
        if (openResult !=1){
            throw new BizException(63004,"操作失败");
        }else {
            return ShopBOConverter.toShopBO(shopMapper.findByBusinessNo(userContextBO.getTenantId(), userContextBO.getUserId(), businessNo));
        }
    }
}
