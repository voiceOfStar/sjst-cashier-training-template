package com.meituan.catering.management.shop.dao.mapper;
import java.util.List;

import com.meituan.catering.management.shop.dao.model.request.SearchShopDataRequest;
import org.apache.ibatis.annotations.Param;

import com.meituan.catering.management.shop.dao.model.ShopDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * 门店对应的MyBatis Mapper
 * @author mac
 */
@Mapper
public interface ShopMapper {

    /**
     * 根据商户号查询门店
     * @param tenantId
     * @param userId
     * @param businessNo
     * @return
     */
    ShopDO findByBusinessNo(Long tenantId, Long userId, String businessNo);

    /**
     * 插入门店信息
     * @param shopDO
     * @return
     */
    int insert(ShopDO shopDO);

    /**
     * 根据ID获取门店信息
     * @param id
     * @return
     */
    ShopDO selectById(@Param("id") Long id);

    /**
     * 根据分页查询请求查询门店概要信息列表
     * @param request
     * @return
     */
    List<ShopDO> searchForPage(SearchShopDataRequest request);

    /**
     * 根据查询请求计算有多少符合条件的门店
     * @param request
     * @return
     */
    int countAll(SearchShopDataRequest request);

    /**
     * 根据门店号更新门店信息
     * @param shopDO
     * @return
     */
    int updateByBusinessNo(ShopDO shopDO);

    /**
     * 关闭门店
     * @param shopDO
     * @return
     */
    int closeShop(ShopDO shopDO);

    /**
     * 开启门店
     * @param shopDO
     * @return
     */
    int openShop(ShopDO shopDO);
}
