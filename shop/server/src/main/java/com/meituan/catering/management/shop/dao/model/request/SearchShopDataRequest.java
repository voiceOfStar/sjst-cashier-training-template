package com.meituan.catering.management.shop.dao.model.request;

import com.meituan.catering.management.common.model.enumeration.BusinessTypeEnum;
import com.meituan.catering.management.shop.api.http.model.enumeration.ManagementTypeEnum;
import lombok.Data;
import java.util.List;

/**
 * 搜索门店的查询条件
 * @author 唐程君
 */
@Data
public class SearchShopDataRequest {
    private Integer pageIndex;

    private Integer pageSize;

    private String keyword;

    private ManagementTypeEnum managementType;

    private BusinessTypeEnum businessType;

    private Boolean enabled;

    private List<SortField> sortFields;

    private Long tenantId;

    private Long userId;

    @Data
    public static class SortField {
        private String field;
        private String direction;
    }
}
