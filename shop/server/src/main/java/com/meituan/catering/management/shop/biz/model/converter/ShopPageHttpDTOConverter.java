package com.meituan.catering.management.shop.biz.model.converter;

import com.meituan.catering.management.common.model.biz.PageBO;
import com.meituan.catering.management.shop.api.http.model.dto.ShopPageHttpDTO;
import com.meituan.catering.management.shop.biz.model.ShopBO;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName ShopPageHttpDTOConverter
 * @Description
 * @Author 唐程君
 * @Date 2022/5/16 11:17
 */
public class ShopPageHttpDTOConverter {
    public static ShopPageHttpDTO toShopPageHttpDTO(PageBO<ShopBO> pageBO){
        ShopPageHttpDTO shopPageHttpDTO = new ShopPageHttpDTO();
        shopPageHttpDTO.setPageIndex(pageBO.getPageIndex());
        shopPageHttpDTO.setPageSize(pageBO.getPageSize());
        shopPageHttpDTO.setTotalCount(pageBO.getTotalCount());
        shopPageHttpDTO.setTotalPageCount(pageBO.getTotalPageCount());
        shopPageHttpDTO.setRecords(buildRecords(pageBO.getRecords()));
        return shopPageHttpDTO;
    }

    public static List<ShopPageHttpDTO.Record> buildRecords(List<ShopBO> shopBOList){
        ArrayList<ShopPageHttpDTO.Record> records = new ArrayList<>();
        for(ShopBO shopBO:shopBOList){
            ShopPageHttpDTO.Record record = new ShopPageHttpDTO.Record();
            record.setId(shopBO.getId());
            record.setTenantId(shopBO.getTenantId());
            record.setVersion(shopBO.getVersion());
            record.setBusinessNo(shopBO.getBusinessNo());
            record.setName(shopBO.getName());
            record.setManagementType(shopBO.getManagementType());
            record.setBusinessType(shopBO.getBusinessType());
            if(shopBO.getBusinessArea() !=null){
                record.setBusinessArea(shopBO.getBusinessArea());
            }
            if(shopBO.getComment() !=null){
                record.setComment(shopBO.getComment());
            }
            record.setEnabled(shopBO.getEnabled());
            record.getOpeningHours().setOpenTime(shopBO.getOpenTime());
            record.getOpeningHours().setCloseTime(shopBO.getCloseTime());
            record.getAuditing().setCreatedAt(shopBO.getAuditing().getCreatedAt());
            record.getAuditing().setCreatedBy(shopBO.getAuditing().getCreatedBy());
            record.getAuditing().setLastModifiedAt(shopBO.getAuditing().getLastModifiedAt());
            record.getAuditing().setLastModifiedBy(shopBO.getAuditing().getLastModifiedBy());
            record.getContact().setName(shopBO.getContactName());
            record.getContact().setAddress(shopBO.getContactAddress());
            if(shopBO.getCellphone() !=null){
                record.getContact().setCellphone(shopBO.getCellphone());
            }
            if(shopBO.getTelephone() !=null){
                record.getContact().setTelephone(shopBO.getTelephone());
            }
            records.add(record);
        }
        return records;
    }
}
