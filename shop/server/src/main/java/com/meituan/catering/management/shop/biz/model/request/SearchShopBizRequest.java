package com.meituan.catering.management.shop.biz.model.request;

import com.meituan.catering.management.common.model.enumeration.BusinessTypeEnum;
import com.meituan.catering.management.shop.api.http.model.enumeration.ManagementTypeEnum;
import com.meituan.catering.management.shop.api.http.model.request.SearchShopHttpRequest;
import lombok.Data;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static java.lang.Boolean.FALSE;

/**
 * @ClassName SearchShopBizRequest
 * @Description
 * @Author 唐程君
 * @Date 2022/5/16 10:18
 */
@Data
public class SearchShopBizRequest {
    private Integer pageIndex;

    private Integer pageSize;

    private Condition condition = new Condition();

    @Data
    public static class Condition {
        private String keyword;
        private ManagementTypeEnum managementType;
        private BusinessTypeEnum businessType;
        private Boolean enabled;

    }

    private List<SortField> sortFields = new LinkedList<>();

    @Data
    public static class SortField {
        private String field;
        private Boolean asc = FALSE;
    }
}
