package com.meituan.catering.management.shop.biz.model.converter;

import com.meituan.catering.management.shop.api.http.model.request.UpdateShopHttpRequest;
import com.meituan.catering.management.shop.biz.model.request.UpdateShopBizRequest;

/**
 * @ClassName UpdateBieRequestConverter
 * @Description
 * @Author 唐程君
 * @Date 2022/5/16 21:21
 */
public class UpdateBizRequestConverter {
    public static UpdateShopBizRequest toUpdateBizRequest(UpdateShopHttpRequest request){
        UpdateShopBizRequest updateShopBizRequest = new UpdateShopBizRequest();
        updateShopBizRequest.setVersion(request.getVersion());
        updateShopBizRequest.setName(request.getName());
        UpdateShopBizRequest.ContactBizModel contact = new UpdateShopBizRequest.ContactBizModel();
        contact.setTelephone(request.getContact().getTelephone());
        contact.setCellphone(request.getContact().getCellphone());
        contact.setName(request.getContact().getName());
        contact.setAddress(request.getContact().getAddress());
        updateShopBizRequest.setContact(contact);
        updateShopBizRequest.setBusinessType(request.getBusinessType());
        updateShopBizRequest.setManagementType(request.getManagementType());
        UpdateShopBizRequest.OpeningHoursTimeRange openingHoursTimeRange = new UpdateShopBizRequest.OpeningHoursTimeRange();
        openingHoursTimeRange.setOpenTime(request.getOpeningHours().getOpenTime());
        openingHoursTimeRange.setCloseTime(request.getOpeningHours().getCloseTime());
        updateShopBizRequest.setOpeningHours(openingHoursTimeRange);
        updateShopBizRequest.setBusinessArea(request.getBusinessArea());
        updateShopBizRequest.setComment(request.getComment());
        return updateShopBizRequest;
    }
}
