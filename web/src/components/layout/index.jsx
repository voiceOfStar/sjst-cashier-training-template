/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable import/no-anonymous-default-export */
import { Layout, Menu, Breadcrumb } from "antd";
import { Link, Routes, Route } from "react-router-dom";
import React from "react";
import Shop from "../../pages/shop/manage";
import Detail from "../../pages/detail";
import Edit from "../../pages/shop/edit";
import Create from "../../pages/shop/create";
import {} from "@ant-design/icons";
import Logo from "../../assets/下载.png";
import './index.css';

const { Header, Content, Sider } = Layout;
const items1 = ["运营中心", "营销中心", "会员中心", "库存管理", "报表中心"].map(
  (key) => ({
    key,
    label: `${key}`,
  })
);
export default () => (
  <Layout>
    <Header className="header" style={{backgroundColor:'white'}}>
      <div className="logo">
        <img src={Logo}/>
      </div>
      <Menu
        theme="light"
        mode="horizontal"
        defaultSelectedKeys={["运营中心"]}
        items={items1}
        style={{marginLeft:'200px'}}
      />
    </Header>
    <Layout>
      <Sider width={200} className="site-layout-background">
        <Menu
          mode="inline"
          style={{
            height: "100%",
            borderRight: 0,
          }}
        >
          <Menu.Item key={1}>
            <Link to="/shop">门店管理</Link>
          </Menu.Item>
          <Menu.Item key={2}>菜品管理</Menu.Item>
          <Menu.Item key={3}>
            <Link to="/detail">订单管理</Link>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout
        style={{
          padding: "0 24px 24px",
        }}
      >
        <Content
          style={{
            padding: 24,
            margin: 0,
            minHeight: 280,
            backgroundColor: "#FFF",
          }}
        >
          <Routes>
            <Route path="/" element={"首页"} />
            <Route path="/shop" element={<Shop />} />
            <Route path="/detail" element={<Detail />} />
            <Route path="/edit/:no" element={<Edit />} />
            <Route path="/create" element={<Create />} />
          </Routes>
        </Content>
      </Layout>
    </Layout>
  </Layout>
);
