import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  TimePicker,
  Button,
  Popconfirm,
  message,
} from "antd";
import {} from "@ant-design/icons";
import moment from "moment";
import "./index.css";
import axios from "axios";

const { Option } = Select;

export default function Edit() {
  let navigate = useNavigate();
  const params = useParams();//通过路由获取门店唯一编号
  const [dataSource, setDataSource] = useState();
  const [shop] = Form.useForm();
  let toShop = true;//保存后是否跳转到主页

  const onFinish = (values) => {
    let updateDataSource = {
      businessArea: values.businessArea,
      businessType: values.businessType,
      comment: values.comment,
      contact: {
        address: values.contactAddress,
        cellphone: values.contactCellphone,
        name: values.contactName,
        telephone: values.contactTelephone,
      },
      managementType: values.managementType,
      name: values.name,
      openingHours: {
        closeTime: moment(values.openingHoursCloseTime._d.getTime()).format(
          "HH:mm"
        ),
        openTime: moment(values.openingHoursOpenTime._d.getTime()).format(
          "HH:mm"
        ),
      },
      version: dataSource.version,
    };
    console.log(updateDataSource);
    const instance = axios.create({
      headers: { tenantId: 500, userId: 11000 },
    });
    instance.put(`/shop/${params.no}`, updateDataSource).then((res) => {
      console.log(res);
      if (res.data.status.code === 0) {
        message.success("修改成功");
        toShop && navigate("/shop");
        toShop || setDataSource(res.data.data);
      } else {
        message.error(res.data.status.msg);
      }
    });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  useEffect(() => {
    const instance = axios.create({
      headers: { tenantId: 500, userId: 11000 },
    });
    dataSource ||
      instance.get(`/shop/${params.no}`).then((res) => {
        console.log(res);
        setDataSource(res.data.data);
      });
    dataSource &&
      shop.setFieldsValue({
        name: dataSource.name,
        businessType: dataSource.businessType,
        managementType: dataSource.managementType,
        contactTelephone: dataSource.contact.telephone,
        contactCellphone: dataSource.contact.cellphone,
        contactName: dataSource.contact.name,
        contactAddress: dataSource.contact.address,
        openingHoursOpenTime: moment(dataSource.openingHours.openTime, "HH:mm"),
        openingHoursCloseTime: moment(
          dataSource.openingHours.closeTime,
          "HH:mm"
        ),
        businessArea: dataSource.businessArea,
        comment: dataSource.comment,
      });
  }, [dataSource]);

  return (
    <>
      <div className="title">编辑 - {dataSource && dataSource.name}</div>
      <Form form={shop} onFinish={onFinish} onFinishFailed={onFinishFailed}>
        <Form.Item>
          <h3>通用信息</h3>
        </Form.Item>
        <Form.Item
          label="门店名"
          name="name"
          style={{ marginBottom: "10px" }}
          rules={[
            { required: true, message: "请输入用户名" },
            {
              validator: (_, value) => {
                if (/^[\u4e00-\u9fa5]{1,50}$/.test(value)) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(new Error("只能输入50位以内的汉字"));
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Row gutter={10}>
          <Col span={12}>
            <Form.Item
              label="主营业态"
              name="businessType"
              rules={[{ required: true }]}
            >
              <Select>
                <Option value="DINNER">正餐</Option>
                <Option value="FAST_FOOD">快餐</Option>
                <Option value="HOT_POT">火锅</Option>
                <Option value="BARBECUE">烧烤</Option>
                <Option value="WESTERN_FOOD">西餐</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="管理类型"
              name="managementType"
              rules={[{ required: true }]}
            >
              <Select>
                <Option value="DIRECT_SALES">直营</Option>
                <Option value="ALLIANCE">加盟</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <h3>联系方式</h3>
        </Form.Item>
        <Row gutter={10} style={{ marginBottom: "0px" }}>
          <Col span={8}>
            <Form.Item
              label="座机号"
              name="contactTelephone"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      /^[0-9]{3}[-][0-9]{8,11}$/.test(value) ||
                      value === undefined ||
                      value === ""
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入15位以内的数字或-")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="手机号"
              name="contactCellphone"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      /^[0-9]{0,2}[-]?[0-9]{11}$/g.test(value) ||
                      value === undefined ||
                      value === ""
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入15位以内的数字或-")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="联系人"
              name="contactName"
              rules={[
                { required: true, message: "请输入联系人" },
                {
                  validator: (_, value) => {
                    if (/^[\u4e00-\u9fa5]+$/.test(value) && value.length <= 8) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(new Error("只能输入8位以内的汉字"));
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label="地址"
          name="contactAddress"
          rules={[
            { required: true, message: "请输入联系地址" },
            {
              validator: (_, value) => {
                if (/^[\u4e00-\u9fa50-9]*$/.test(value) && value.length <= 30) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(
                    new Error("只能输入30位以内的汉字或数字")
                  );
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item>
          <h3>经营信息</h3>
        </Form.Item>
        <Row gutter={10} style={{ marginBottom: "-10px" }}>
          <Col>
            <Form.Item
              label="营业时间"
              name="openingHoursOpenTime"
              rules={[{ required: true, message: "请输入营业开始时间" }]}
            >
              <TimePicker placeholder="开始时间" format="HH:mm" />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="openingHoursCloseTime"
              rules={[{ required: true, message: "请输入营业结束时间" }]}
            >
              <TimePicker placeholder="结束时间" format="HH:mm" />
            </Form.Item>
          </Col>
          <Col span={17}>
            <Form.Item
              name="businessArea"
              label="门店面积"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      (/^[\u4e00-\u9fa50-9]*$/.test(value) &&
                        value.length <= 30) ||
                      value === "" ||
                      value === undefined
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入30位以内的汉字或数字")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label="门店介绍"
          name="comment"
          rules={[
            {
              validator: (_, value) => {
                if (
                  (/^[\u4e00-\u9fa50-9，。！]*$/.test(value) &&
                    value.length <= 100) ||
                  value === "" ||
                  value === undefined
                ) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(
                    new Error("只能输入100位以内的汉字或数字")
                  );
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <hr />
        <Row justify="end">
          <Col>
            <Form.Item>
              <Popconfirm
                title="确认放弃修改的内容?"
                onCancel={() => {
                  navigate("/shop");
                }}
                okText="回到当前界面"
                cancelText="返回主界面"
              >
                <Button className="button">取消</Button>
              </Popconfirm>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => {
                  toShop=true;
                  shop.submit();
                }}
                className="button"
                style={{
                  backgroundColor: "orange",
                }}
              >
                保存并返回
              </Button>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => {
                  toShop = false;
                  shop.submit();
                }}
                className="button"
                style={{
                  backgroundColor: "orange",
                  marginRight: "0px",
                }}
              >
                保存
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
}
