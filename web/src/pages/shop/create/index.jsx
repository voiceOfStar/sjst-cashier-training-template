import React from "react";
import { useNavigate } from "react-router-dom";
import {
  Row,
  Col,
  Form,
  Select,
  Input,
  TimePicker,
  Button,
  Popconfirm,
  message,
} from "antd";
import {} from "@ant-design/icons";
import moment from "moment";
import axios from "axios";

const { Option } = Select;
export default function Create() {
  let navigate = useNavigate();
  let toShop = true;//保存后是否跳转到主页

  const [createForm] = Form.useForm();

  const onFinish = (values) => {
    const instance = axios.create({
      headers: { tenantId: 500, userId: 11000 },
    });
    instance
      .post("/shop", {
        businessArea: values.businessArea,
        businessType: values.businessType,
        comment: values.comment,
        contact: {
          address: values.contactAddress,
          cellphone: values.contactCellphone,
          name: values.contactName,
          telephone: values.contactTelephone,
        },
        managementType: values.managementType,
        name: values.name,
        openingHours: {
          closeTime: moment(values.openingHoursCloseTime._d.getTime()).format(
            "HH:mm"
          ),
          openTime: moment(values.openingHoursOpenTime._d.getTime()).format(
            "HH:mm"
          ),
        },
      })
      .then((res) => {
        console.log(res);
        if (res.data.status.code === 0) {
          message.success("创建成功");
          toShop && navigate("/shop");
        } else {
          message.error(res.data.status.msg);
        }
      });
  };
  return (
    <>
      <div className="title">新门店</div>
      <Form
        form={createForm}
        onFinish={onFinish}
        initialValues={{
          businessType: "DINNER",
          managementType: "DIRECT_SALES",
        }}
      >
        <Form.Item>
          <h3>通用信息</h3>
        </Form.Item>
        <Form.Item
          label="门店名"
          name="name"
          style={{ marginBottom: "10px" }}
          rules={[
            { required: true, message: "请输入门店名" },
            {
              validator: (_, value) => {
                if (/^[\u4e00-\u9fa5]{1,50}$/.test(value)) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(new Error("只能输入50位以内的汉字"));
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Row gutter={10}>
          <Col span={12}>
            <Form.Item
              label="主营业态"
              name="businessType"
              rules={[{ required: true }]}
            >
              <Select>
                <Option value="DINNER">正餐</Option>
                <Option value="FAST_FOOD">快餐</Option>
                <Option value="HOT_POT">火锅</Option>
                <Option value="BARBECUE">烧烤</Option>
                <Option value="WESTERN_FOOD">西餐</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label="管理类型"
              name="managementType"
              rules={[{ required: true }]}
            >
              <Select>
                <Option value="DIRECT_SALES">直营</Option>
                <Option value="ALLIANCE">加盟</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.Item>
          <h3>联系方式</h3>
        </Form.Item>
        <Row gutter={10} style={{ marginBottom: "0px" }}>
          <Col span={8}>
            <Form.Item
              label="座机号"
              name="contactTelephone"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      /^[0-9]{3}[-][0-9]{8,11}$/.test(value) ||
                      value === undefined ||
                      value === ""
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入15位以内的数字或-")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="手机号"
              name="contactCellphone"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      /^[0-9]{0,2}[-]?[0-9]{11}$/g.test(value) ||
                      value === undefined ||
                      value === ""
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入15位以内的数字或-")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="联系人"
              name="contactName"
              rules={[
                { required: true, message: "请输入联系人" },
                {
                  validator: (_, value) => {
                    if (/^[\u4e00-\u9fa5]+$/.test(value) && value.length <= 8) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(new Error("只能输入8位以内的汉字"));
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label="地址"
          name="contactAddress"
          rules={[
            { required: true, message: "请输入地址" },
            {
              validator: (_, value) => {
                if (/^[\u4e00-\u9fa50-9]*$/.test(value) && value.length <= 30) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(
                    new Error("只能输入30位以内的汉字或数字")
                  );
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item>
          <h3>经营信息</h3>
        </Form.Item>
        <Row gutter={10} style={{ marginBottom: "-10px" }}>
          <Col>
            <Form.Item
              label="营业时间"
              name="openingHoursOpenTime"
              rules={[{ required: true, message: "请选择营业时间" }]}
            >
              <TimePicker placeholder="开始时间" />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              name="openingHoursCloseTime"
              rules={[{ required: true, message: "请选择营业时间" }]}
            >
              <TimePicker placeholder="结束时间" />
            </Form.Item>
          </Col>
          <Col span={17}>
            <Form.Item
              name="businessArea"
              label="门店面积"
              rules={[
                {
                  validator: (_, value) => {
                    if (
                      (/^[\u4e00-\u9fa50-9]*$/.test(value) &&
                        value.length <= 30) ||
                      value === "" ||
                      value === undefined
                    ) {
                      return Promise.resolve();
                    } else {
                      return Promise.reject(
                        new Error("只能输入30位以内的汉字或数字")
                      );
                    }
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>
        <Form.Item
          label="门店介绍"
          name="comment"
          rules={[
            {
              validator: (_, value) => {
                if (
                  (/^[\u4e00-\u9fa50-9，。！]*$/.test(value) &&
                    value.length <= 100) ||
                  value === "" ||
                  value === undefined
                ) {
                  return Promise.resolve();
                } else {
                  return Promise.reject(
                    new Error("只能输入100位以内的汉字或数字")
                  );
                }
              },
            },
          ]}
        >
          <Input />
        </Form.Item>
        <hr />
        <Row justify="end">
          <Col>
            <Form.Item>
              <Popconfirm
                title="确认放弃修改的内容?"
                onCancel={() => {
                  navigate("/shop");
                }}
                okText="回到当前界面"
                cancelText="返回主界面"
              >
                <Button className="button">取消</Button>
              </Popconfirm>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => {
                  toShop=true;
                  createForm.submit();
                }}
                className="button"
                style={{
                  backgroundColor: "orange",
                }}
              >
                保存并返回
              </Button>
            </Form.Item>
          </Col>
          <Col>
            <Form.Item>
              <Button
                onClick={() => {
                  toShop = false;
                  createForm.submit();
                }}
                className="button"
                style={{
                  backgroundColor: "orange",
                  marginRight: "0px",
                }}
              >
                保存
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </>
  );
}
