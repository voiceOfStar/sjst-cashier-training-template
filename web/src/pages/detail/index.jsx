import { Button } from "antd";
import { Link,useNavigate } from "react-router-dom";    
export default function Detail() {
    let navigate = useNavigate();
  return (
    <>
      <Button onClick={() =>{
          navigate("/");
      }}>
        <Link to="/">跳转到首页</Link>
      </Button>
      <Button onClick={() =>{
          navigate("/shop");
      }}>
        跳转到门店
      </Button>
    </>
  );
}
