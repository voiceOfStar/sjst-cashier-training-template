import './App.css';
import 'antd/dist/antd.min.css';
import Layout from './components/layout/index';

function App() {
  return (
    <div className="App">
      <Layout/>
    </div>
  );
}

export default App;